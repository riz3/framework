<?php
/**
 * The template for displaying archive pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package
 * @subpackage
 * @since
 */

get_header(); ?>

<div id="page-container">
    <div class="blog-banner" style="background-image: url('<?php echo get_template_directory_uri();?>/library/images/beach.jpg')">
        <div class="overlay"></div><!-- .overlay -->
        <div class="wrap">
            <h1><?php the_archive_title(); ?></h1>
        </div><!-- .wrap -->
    </div><!-- .blog-banner -->

    <div class="blog-content-container">
        <div class="wrap">
            <div class="row">
                <div class="col-2-3">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <div class="post-excerpt-container">
                            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                            <?php the_date(); ?>
                            <a class="thumbnail-link" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('large'); ?></a>
                            <?php the_excerpt(); ?>
                        </div><!-- .post-excerpt-container -->
                    <?php endwhile; else : ?>
                        <article id="post-not-found" class="hentry clearfix">
                            <header class="article-header">
                                <h1><?php _e( 'Hmmm, what are you looking for?', 'bonestheme' ); ?></h1>
                            </header>
                            <section class="entry-content">
                                <p><?php _e( 'Nothing was found based on your search.', 'bonestheme' ); ?></p>
                            </section>
                            <footer class="article-footer">
                                    <p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
                            </footer>
                        </article>
                    <?php endif; ?>
                </div><!-- .col-2-3 -->
                <div class="col-1-3">
                    <?php get_sidebar(); ?>
                </div><!-- .col-1-3 -->
            </div><!-- .row-->
        </div><!-- .wrap -->
    </div><!-- .blog-content-container -->
</div><!-- #page-container-->
<?php get_footer(); ?>
