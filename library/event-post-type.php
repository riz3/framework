<?php
/*
Event Post Type

Developed by:
URL:
*/

// let's create the function for the custom type
function custom_post_type() {
	$name = 'Event';
	$name_plural = 'Events';
	$icon = 'dashicons-calendar-alt';
	$slug = 'events';
	$tag = 'event_tag';
	$cat = 'event_category';

	// creating the custom type
	register_post_type( $slug,
		array( 'labels' => array(
			'name' => __( $name_plural, 'riz3theme' ), /* This is the Title of the Group */
			'singular_name' => __( $name, 'riz3theme' ), /* This is the individual type */
			'all_items' => __( "All $name_plural", 'riz3theme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'riz3theme' ), /* The add new menu item */
			'add_new_item' => __( "Add New $name", 'riz3theme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( "Edit $name_plural", 'riz3theme' ), /* Edit Display Title */
			'new_item' => __( "New $name", 'riz3theme' ), /* New Display Title */
			'view_item' => __( "View $name", 'riz3theme' ), /* View Display Title */
			'search_items' => __( "Search $name_plural", 'riz3theme' ), /* Search Custom Type Title */
			'not_found' =>  __( "There are currently no $name_plural.", 'riz3theme' ), /* This displays if there are no entries yet */
			'not_found_in_trash' => __( 'Nothing found in Trash', 'riz3theme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( "This is the custom post type for $events_plural", 'riz3theme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => $icon, /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => $slug, 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => $slug, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'revisions', 'sticky')
		) /* end of options */
	); /* end of register post type */

	/* this adds your post categories to your custom post type */
	register_taxonomy_for_object_type( $cat, $slug );
	/* this adds your post tags to your custom post type */
	register_taxonomy_for_object_type( $tag, $slug );

	// now let's add custom categories
	register_taxonomy( $cat,
		array($slug), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( "$name Categories", 'riz3theme' ), /* name of the custom taxonomy */
				'singular_name' => __( "$name Category", 'riz3theme' ), /* single taxonomy name */
				'search_items' =>  __( "Search $name Categories", 'riz3theme' ), /* search title for taxomony */
				'all_items' => __( "All $name Categories", 'riz3theme' ), /* all title for taxonomies */
				'parent_item' => __( "Parent $name Category", 'riz3theme' ), /* parent title for taxonomy */
				'parent_item_colon' => __( "Parent $name Category:", 'riz3theme' ), /* parent taxonomy title */
				'edit_item' => __( "Edit $name Category", 'riz3theme' ), /* edit custom taxonomy title */
				'update_item' => __( "Update $name Category", 'riz3theme' ), /* update title for taxonomy */
				'add_new_item' => __( "Add New $name Category", 'riz3theme' ), /* add new title for taxonomy */
				'new_item_name' => __( "New $name Category Name", 'riz3theme' ) /* name title for taxonomy */
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,
		)
	);

	// now let's add custom tags
	register_taxonomy( $tag,
		array($slug), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
		array('hierarchical' => false,    /* if this is false, it acts like tags */
			'labels' => array(
				'name' => __( "$name Tags", 'riz3theme' ), /* name of the custom taxonomy */
				'singular_name' => __( "$name Tag", 'riz3theme' ), /* single taxonomy name */
				'search_items' =>  __( "Search $name Tags", 'riz3theme' ), /* search title for taxomony */
				'all_items' => __( "All $name Tags", 'riz3theme' ), /* all title for taxonomies */
				'parent_item' => __( "Parent $name Tag", 'riz3theme' ), /* parent title for taxonomy */
				'parent_item_colon' => __( "Parent $name Tag:", 'riz3theme' ), /* parent taxonomy title */
				'edit_item' => __( "Edit $name Tag", 'riz3theme' ), /* edit custom taxonomy title */
				'update_item' => __( "Update $name Tag", 'riz3theme' ), /* update title for taxonomy */
				'add_new_item' => __( "Add New $name Tag", 'riz3theme' ), /* add new title for taxonomy */
				'new_item_name' => __( "New $name Tag Name", 'riz3theme' ) /* name title for taxonomy */
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,
		)
	);
}

// adding the function to the Wordpress init
add_action( 'init', 'custom_post_type');

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'event_flush_rewrite_rules' );

// Flush your rewrite rules
function event_flush_rewrite_rules() {
	flush_rewrite_rules();
}



/***********************************************
Include custom meta data here using CMB2
************************************************/


add_action( 'cmb2_admin_init', 'riz3_event_options' );
/**
 * Hook in and register a metabox to handle an options page and adds a menu item.
 */
function riz3_event_options() {
	$prefix = 'event_';
	/**
	 * Registers options page menu item and form.
	 */
	$cmb_options = new_cmb2_box( array(
		'id'           => $prefix . 'option_metabox',
		'title'        => esc_html__( 'Event Options', 'myprefix' ),
		'object_types' => array( 'options-page' ),
		/*
		 * The following parameters are specific to the options-page box
		 * Several of these parameters are passed along to add_menu_page()/add_submenu_page().
		 */
		'option_key'      => $prefix . 'options', // The option key and admin menu page slug.
		// 'icon_url'        => 'dashicons-palmtree', // Menu icon. Only applicable if 'parent_slug' is left empty.
		// 'menu_title'      => esc_html__( 'Options', 'myprefix' ), // Falls back to 'title' (above).
		'parent_slug'     => 'edit.php?post_type=events', // Make options page a submenu item of the themes menu.
		// 'capability'      => 'manage_options', // Cap required to view options-page.
		// 'position'        => 1, // Menu position. Only applicable if 'parent_slug' is left empty.
		// 'admin_menu_hook' => 'network_admin_menu', // 'network_admin_menu' to add network-level options page.
		// 'display_cb'      => false, // Override the options-page form output (CMB2_Hookup::options_page_output()).
		// 'save_button'     => esc_html__( 'Save Theme Options', 'myprefix' ), // The text for the options-page save button. Defaults to 'Save'.
	) );
	/*
	 * Options fields ids only need
	 * to be unique within this box.
	 * Prefix is not needed.
	 */
 	$cmb_options->add_field( array(
		'name' => esc_html__( 'Events Page Title', 'cmb2' ),
		'desc' => esc_html__( 'This will be displayed on the event archive page', 'cmb2' ),
		'id'   => $prefix . 'title',
		'type' => 'text',
 	));
	$cmb_options->add_field( array(
		'name' => esc_html__( 'Events Page Subtitle', 'cmb2' ),
		'desc' => esc_html__( 'This subtitle will be displayed on the event archive page', 'cmb2' ),
		'id'   => $prefix . 'sub_title',
		'type' => 'text',
 	));
	$cmb_options->add_field( array(
 		'name' => esc_html__( 'Banner Image', 'cmb2' ),
 		'desc' => esc_html__( 'Upload an image or enter a URL.', 'cmb2' ),
 		'id'   => $prefix . 'image',
 		'type' => 'file',
 	));
}

//Callback function used to display custom events fields on the front-end
function event_get_options( $key = '', $default = false ) {
	if ( function_exists( 'cmb2_get_option' ) ) {
		// Use cmb2_get_option as it passes through some key filters.
		return cmb2_get_option( 'event_options', $key, $default );
	}
	// Fallback to get_option if CMB2 is not loaded yet.
	$opts = get_option( 'event_options', $default );
	$val = $default;
	if ( 'all' == $key ) {
		$val = $opts;
	} elseif ( is_array( $opts ) && array_key_exists( $key, $opts ) && false !== $opts[ $key ] ) {
		$val = $opts[ $key ];
	}
	return $val;
}//End Function (event_get_options)

?>
