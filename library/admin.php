<?php
/*
This file handles the admin area and functions.
You can use this file to make changes to the
dashboard.

Developed by:
URL:

*/

/************* DASHBOARD WIDGETS *****************/

// disable default dashboard widgets
function disable_default_dashboard_widgets() {
	// remove_meta_box( 'dashboard_right_now', 'dashboard', 'core' );    // Right Now Widget
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'core' ); // Comments Widget
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'core' );  // Incoming Links Widget
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'core' );         // Plugins Widget

	// remove_meta_box('dashboard_quick_press', 'dashboard', 'core' );   // Quick Press Widget
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'core' );   // Recent Drafts Widget
	remove_meta_box( 'dashboard_primary', 'dashboard', 'core' );         //
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'core' );       //

	// removing plugin dashboard boxes
	remove_meta_box( 'yoast_db_widget', 'dashboard', 'normal' );         // Yoast's SEO Plugin Widget
}

/*
Now let's talk about adding your own custom Dashboard widget.
Sometimes you want to show clients feeds relative to their
site's content. For example, the NBA.com feed for a sports
site.

For more information on creating Dashboard Widgets, view:
http://digwp.com/2010/10/customize-wordpress-dashboard/
*/

//Dashboard Widget
function riz3_dashboard_widget() {
}

// calling all custom dashboard widgets
function riz3_custom_dashboard_widgets() {
	wp_add_dashboard_widget( 'riz3s_dashboard_widget', __( 'Custom Dashboard Widget (Customize on admin.php)', 'riz3theme' ), 'riz3_dashboard_widget' );
	/*
	Be sure to drop any other created Dashboard Widgets
	in this function and they will all load.
	*/
}

// adding any custom widgets
add_action( 'wp_dashboard_setup', 'riz3_custom_dashboard_widgets' );
// removing the dashboard widgets
add_action( 'admin_menu', 'disable_default_dashboard_widgets' );


/************* CUSTOM LOGIN PAGE *****************/

// calling your own login css so you can style it
function riz3_login_css() {
	wp_enqueue_style( 'riz3_login_css', get_template_directory_uri() . '/library/css/login.css', false );
}

// changing the logo link from wordpress.org to your site
function riz3_login_url() {  return home_url(); }
// changing the alt text on the logo to show your site name
function riz3_login_title() { return get_option( 'blogname' ); }

// calling it only on the login page
add_action( 'login_enqueue_scripts', 'riz3_login_css', 10 );
add_filter( 'login_headerurl', 'riz3_login_url' );
add_filter( 'login_headertitle', 'riz3_login_title' );


?>
