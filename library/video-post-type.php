<?php
/*
Event Post Type

Developed by:
URL:
*/

// let's create the function for the custom type
function video_post_type() {
	$name = 'Video';
	$name_plural = 'Videos';
	$icon = 'dashicons-video-alt';
	$slug = 'videos';
	$tag = 'video_tag';
	$cat = 'video_category';

	// creating the custom type
	register_post_type( $slug,
		array( 'labels' => array(
			'name' => __( $name_plural, 'riz3theme' ), /* This is the Title of the Group */
			'singular_name' => __( $name, 'riz3theme' ), /* This is the individual type */
			'all_items' => __( "All $name_plural", 'riz3theme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'riz3theme' ), /* The add new menu item */
			'add_new_item' => __( "Add New $name", 'riz3theme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( "Edit $name_plural", 'riz3theme' ), /* Edit Display Title */
			'new_item' => __( "New $name", 'riz3theme' ), /* New Display Title */
			'view_item' => __( "View $name", 'riz3theme' ), /* View Display Title */
			'search_items' => __( "Search $name_plural", 'riz3theme' ), /* Search Custom Type Title */
			'not_found' =>  __( "There are currently no $name_plural.", 'riz3theme' ), /* This displays if there are no entries yet */
			'not_found_in_trash' => __( 'Nothing found in Trash', 'riz3theme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( "This is the custom post type for $events_plural", 'riz3theme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => $icon, /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => $slug, 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => $slug, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'revisions', 'sticky')
		) /* end of options */
	); /* end of register post type */

	/* this adds your post categories to your custom post type */
	register_taxonomy_for_object_type( $cat, $slug );
	/* this adds your post tags to your custom post type */
	register_taxonomy_for_object_type( $tag, $slug );

	// now let's add custom categories
	register_taxonomy( $cat,
		array($slug), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( "$name Categories", 'riz3theme' ), /* name of the custom taxonomy */
				'singular_name' => __( "$name Category", 'riz3theme' ), /* single taxonomy name */
				'search_items' =>  __( "Search $name Categories", 'riz3theme' ), /* search title for taxomony */
				'all_items' => __( "All $name Categories", 'riz3theme' ), /* all title for taxonomies */
				'parent_item' => __( "Parent $name Category", 'riz3theme' ), /* parent title for taxonomy */
				'parent_item_colon' => __( "Parent $name Category:", 'riz3theme' ), /* parent taxonomy title */
				'edit_item' => __( "Edit $name Category", 'riz3theme' ), /* edit custom taxonomy title */
				'update_item' => __( "Update $name Category", 'riz3theme' ), /* update title for taxonomy */
				'add_new_item' => __( "Add New $name Category", 'riz3theme' ), /* add new title for taxonomy */
				'new_item_name' => __( "New $name Category Name", 'riz3theme' ) /* name title for taxonomy */
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,
		)
	);

	// now let's add custom tags
	register_taxonomy( $tag,
		array($slug), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
		array('hierarchical' => false,    /* if this is false, it acts like tags */
			'labels' => array(
				'name' => __( "$name Tags", 'riz3theme' ), /* name of the custom taxonomy */
				'singular_name' => __( "$name Tag", 'riz3theme' ), /* single taxonomy name */
				'search_items' =>  __( "Search $name Tags", 'riz3theme' ), /* search title for taxomony */
				'all_items' => __( "All $name Tags", 'riz3theme' ), /* all title for taxonomies */
				'parent_item' => __( "Parent $name Tag", 'riz3theme' ), /* parent title for taxonomy */
				'parent_item_colon' => __( "Parent $name Tag:", 'riz3theme' ), /* parent taxonomy title */
				'edit_item' => __( "Edit $name Tag", 'riz3theme' ), /* edit custom taxonomy title */
				'update_item' => __( "Update $name Tag", 'riz3theme' ), /* update title for taxonomy */
				'add_new_item' => __( "Add New $name Tag", 'riz3theme' ), /* add new title for taxonomy */
				'new_item_name' => __( "New $name Tag Name", 'riz3theme' ) /* name title for taxonomy */
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,
		)
	);
}

// adding the function to the Wordpress init
add_action( 'init', 'video_post_type');

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'riz3_flush_rewrite_rules' );

// Flush your rewrite rules
function riz3_flush_rewrite_rules() {
	flush_rewrite_rules();
}

/*
for more information on taxonomies, go here:
http://codex.wordpress.org/Function_Reference/register_taxonomy
*/

/*
	looking for custom meta boxes?
	check out this fantastic tool:
	https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
*/
?>
