<?php
/* Welcome
This is the core-functions file where most of the
main functions & features reside. If you have
any custom functions, it's best to put them
in the functions.php file.

Developed by:
URL:
*/

/*********************
LAUNCH Core Functions
*********************/

// we're firing all out initial functions at the start
add_action( 'after_setup_theme', 'riz3_up', 16 );

function riz3_up() {

	// launching operation cleanup
	add_action( 'init', 'riz3_head_cleanup' );
	// remove WP version from RSS
	add_filter( 'the_generator', 'bones_rss_version' );
	// remove pesky injected css for recent comments widget
	add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );
	// clean up comment styles in the head
	add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );
	// clean up gallery output in wp
	add_filter( 'gallery_style', 'bones_gallery_style' );

	// enqueue base scripts and styles
	add_action( 'wp_enqueue_scripts', 'riz3_scripts_and_styles', 999 );

	// launch custom theme setup
	riz3_theme_support();
	remove_admin_bar();

	// adding sidebars to Wordpress (these are created in core-functions.php)
	add_action( 'widgets_init', 'riz3_register_sidebars' );

	// cleaning up random code around images
	add_filter( 'the_content', 'bones_filter_ptags_on_images' );
	// cleaning up excerpt
	add_filter( 'excerpt_more', 'riz3_excerpt_more' );

} /* end riz3 up */

/*********************
WP_HEAD GOODNESS
The default wordpress head is
a mess. Let's clean it up by
removing all the junk we don't
need.
*********************/

function riz3_head_cleanup() {
	// category feeds
	// remove_action( 'wp_head', 'feed_links_extra', 3 );
	// post and comment feeds
	// remove_action( 'wp_head', 'feed_links', 2 );
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// index link
	remove_action( 'wp_head', 'index_rel_link' );
	// previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// WP version
	remove_action( 'wp_head', 'wp_generator' );
	// remove WP version from css
	add_filter( 'style_loader_src', 'bones_remove_wp_ver_css_js', 9999 );
	// remove Wp version from scripts
	add_filter( 'script_loader_src', 'bones_remove_wp_ver_css_js', 9999 );

} /* end riz3 head cleanup */

// remove WP version from RSS
function bones_rss_version() { return ''; }

// remove WP version from scripts
function bones_remove_wp_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}

// remove injected CSS for recent comments widget
function bones_remove_wp_widget_recent_comments_style() {
	if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
		remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
	}
}

// remove injected CSS from recent comments widget
function bones_remove_recent_comments_style() {
	global $wp_widget_factory;
	if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
		remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
	}
}

// remove injected CSS from gallery
function bones_gallery_style($css) {
	return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
}


/*********************
SCRIPTS & ENQUEUEING
*********************/

// loading modernizr and jquery, and reply script
function riz3_scripts_and_styles() {
	global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
	if (!is_admin()) {

		// register main stylesheet
		wp_register_style( 'riz3-stylesheet', get_template_directory_uri() . '/library/css/style.css', array(), '', 'all' );
		// Fonts
		wp_register_style( 'riz3-fonts', get_template_directory_uri() . '/library/fonts/eurostile_cond/stylesheet.css', array(), '', 'all');
		// ie-only style sheet
		wp_register_style( 'riz3-ie-only', get_template_directory_uri() . '/library/css/ie.css', array(), '' );

		// comment reply script for threaded comments
		if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
			wp_enqueue_script( 'comment-reply' );
		}


		// modernizr (without media query polyfill)
		wp_register_script( 'boxlayout-modernizr', get_template_directory_uri() . '/library/js/modernizr.custom.js', array(), false );
		//adding custom scripts file in the footer
		wp_register_script( 'riz3-js', get_template_directory_uri() . '/library/js/scripts.js', array( 'jquery' ), '', true );
		//Font Awesome script
		wp_register_script( 'font-awesome', 'https://use.fontawesome.com/cb2129f865.js', array( 'jquery' ), '', true );
		//Google Analytics script
		wp_register_script( 'google-analytics', get_template_directory_uri() . '/library/js/google-analytics.js', array(), '', true );


		// enqueue styles and scripts
		wp_enqueue_style( 'riz3-stylesheet' );
		//wp_enqueue_style( 'riz3-fonts' );
		wp_enqueue_style( 'riz3-ie-only' );

		$wp_styles->add_data( 'riz3-ie-only', 'conditional', 'lt IE 9' ); // add conditional wrapper around ie stylesheet

		wp_enqueue_script( 'boxlayout-modernizr' );
		wp_enqueue_script( 'riz3-js' );
		wp_enqueue_script( 'font-awesome' );
		//wp_enqueue_script( 'google-analytics' );
	}
}

/*********************
THEME SUPPORT
*********************/

//Check if woocommerce is activated
if ( ! function_exists( 'is_woocommerce_activated' ) ) {
	function is_woocommerce_activated() {
		if ( class_exists( 'woocommerce' ) ) { return true; } else { return false; }
	}
}

add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );
function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 12;
  return $cols;
}

//Remove admin bar and block wp-admin page except for admin
function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
	  show_admin_bar(false);
	}
}
//Remove toolbar for admin
add_filter('show_admin_bar', '__return_false');

//Redirect non-admin users to home page
add_action( 'admin_init', 'redirect_non_admin_users' );
function redirect_non_admin_users() {
    $file = basename($_SERVER['PHP_SELF']);
    if ($file == 'wp-login.php' || is_admin() && !current_user_can('edit_posts') && $file != 'admin-ajax.php'){
        wp_redirect( home_url() );
        exit();
    }
}

//Redirect non admin users to homepage from dashboard
add_action( 'template_redirect', 'riz3_admin_denied' );
function riz3_admin_denied()
{
    is_admin()
    and ! current_user_can( 'manage_options' )
        and exit( wp_redirect( home_url(), 302 ) );
}

function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


// Add Functions & Theme Support
function riz3_theme_support() {

	//Enable single product slider
	add_theme_support( 'woocommerce' );
	//add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );

	// wp thumbnails (sizes handled in functions.php)
	add_theme_support( 'post-thumbnails' );

	// default thumb size
	set_post_thumbnail_size(125, 125, true);

	if ( function_exists( 'add_image_size' ) ) {
		add_image_size( 'bio-thumb', 300, 300, true ); //(cropped)
	}

	// wp custom background (thx to @bransonwerner for update)
	add_theme_support( 'custom-background',
		array(
		'default-image' => '',  // background image default
		'default-color' => '', // background color default (dont add the #)
		'wp-head-callback' => '_custom_background_cb',
		'admin-head-callback' => '',
		'admin-preview-callback' => ''
		)
	);

	// to add header image support go here: http://themble.com/support/adding-header-background-image-support/

	// uncomment to add post format support...these template pages do not exist and will need to be added.
	/*add_theme_support( 'post-formats',
		array(
			'aside',             // title less blurb
			'gallery',           // gallery of images
			'link',              // quick link to other site
			'image',             // an image
			'quote',             // a quick quote
			'status',            // a Facebook like status update
			'video',             // video
			'audio',             // audio
			'chat'               // chat transcript
		)
	); */

	// wp menus
	add_theme_support( 'menus' );
	// register menus
	register_nav_menus(
		array(
			'main-menu' => __( 'The Main Menu', 'riz3theme' ),      // main menu in header
			'mobile-menu' => __( 'Mobile Menu', 'riz3theme' ),      // mobile flyout menu
			'footer-menu' => __( 'Footer Menu', 'riz3theme' )       // footer menu
		)
	);
} /* end riz3 theme support */


/*********************
MENUS & NAVIGATION
*********************/

// the main menu
function riz3_main_menu() {
	wp_nav_menu(array(
		'container_class' => 'main-menu-container',     // custom div container class (wraps ul)
		'menu_id' => 'main-menu',                       // custom ul id
		'menu' => __( 'The Main Menu', 'riz3' ),        // wp dashboard menu name
		'menu_class' => 'clearfix',                     // adding custom nav class
		'theme_location' => 'main-menu',                // use this for theme integration
		'before' => '',                                 // before the menu
		'after' => '',                                  // after the menu
		'link_before' => '',                            // before each link
		'link_after' => '',                             // after each link
		'depth' => 0,                                   // limit the depth of the nav
		'fallback_cb' => 'riz3_fallback_menu'           // fallback function
	));
} /* end main menu */

// mobile menu
function riz3_mobile_menu() {
	wp_nav_menu(array(
		'container_class' => 'mobile-menu-container',   // class of container
		'menu' => __( 'Mobile Menu', 'riz3' ),          // nav name
		'menu_class' => 'clearfix',                     // adding custom nav class
		'theme_location' => 'mobile-menu',              // where it's located in the theme
		'before' => '',                                 // before the menu
		'after' => '',                                  // after the menu
		'link_before' => '',                            // before each link
		'link_after' => '',                             // after each link
		'depth' => 0,                                   // limit the depth of the nav
		'fallback_cb' => 'riz3_fallback_menu'           // fallback function
	));
} /* end mobile menu */

// footer menu
function riz3_footer_menu() {
	wp_nav_menu(array(
		'container_class' => 'footer-menu-container ',  // class of container
		'menu' => __( 'Footer Menu', 'riz3' ),          // nav name
		'menu_class' => 'clearfix',                     // adding custom nav class
		'theme_location' => 'footer-menu',              // where it's located in the theme
		'before' => '',                                 // before the menu
		'after' => '',                                  // after the menu
		'link_before' => '',                            // before each link
		'link_after' => '',                             // after each link
		'depth' => 0,                                   // limit the depth of the nav
		'fallback_cb' => 'riz3_fallback_menu'           // fallback function
	));
} /* end footer menu */

// this is the fallback menu
function riz3_fallback_menu() {
	wp_page_menu( array(
		'show_home' => true,
		'menu_class' => 'nav clearfix',                 // adding custom nav class
		'include'     => '',
		'exclude'     => '',
		'echo'        => true,
		'link_before' => '',                            // before each link
		'link_after' => ''                              // after each link
	) );
} /* end fallback menu */


/**
 * Get taxonomies terms links.
 *
 * @see get_object_taxonomies()
 */
function riz3_custom_taxonomies_terms_links() {
    // Get post by post ID.
    $post = get_post( $post->ID );

    // Get post type by post.
    $post_type = $post->post_type;

    // Get post type taxonomies.
    $taxonomies = get_object_taxonomies( $post_type, 'objects' );

    $out = array();

    foreach ( $taxonomies as $taxonomy_slug => $taxonomy ){

        // Get the terms related to post.
        $terms = get_the_terms( $post->ID, $taxonomy_slug );

        if ( ! empty( $terms ) ) {
            $out[] = "<h4>Category:</h4>\n<ul>";
            foreach ( $terms as $term ) {
                $out[] = sprintf( '<li>%2$s</li>',
                    esc_url( get_term_link( $term->slug, $taxonomy_slug ) ),
                    esc_html( $term->name )
                );
            }
            $out[] = "\n</ul>\n";
        }
    }
    return implode( '', $out );
}


/*********************
RELATED POSTS FUNCTION
*********************/

// Related Posts Function (call using bones_related_posts(); )
function bones_related_posts() {
	echo '<ul id="bones-related-posts">';
	global $post;
	$tags = wp_get_post_tags( $post->ID );
	if($tags) {
		foreach( $tags as $tag ) {
			$tag_arr .= $tag->slug . ',';
		}
		$args = array(
			'tag' => $tag_arr,
			'numberposts' => 5, /* you can change this to show more */
			'post__not_in' => array($post->ID)
		);
		$related_posts = get_posts( $args );
		if($related_posts) {
			foreach ( $related_posts as $post ) : setup_postdata( $post ); ?>
				<li class="related_post"><a class="entry-unrelated" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
			<?php endforeach; }
		else { ?>
			<?php echo '<li class="no_related_post">' . __( 'No Related Posts Yet!', 'bonestheme' ) . '</li>'; ?>
		<?php }
	}
	wp_reset_query();
	echo '</ul>';
} /* end bones related posts function */




/*********************
PAGE NAVI
*********************/

// Numeric Page Navi (built into the theme by default)
function bones_page_navi() {
	global $wp_query;
	$bignum = 999999999;
	if ( $wp_query->max_num_pages <= 1 )
		return;

	echo '<nav class="pagination">';

		echo paginate_links( array(
			'base' 			=> str_replace( $bignum, '%#%', esc_url( get_pagenum_link($bignum) ) ),
			'format' 		=> '',
			'current' 		=> max( 1, get_query_var('paged') ),
			'total' 		=> $wp_query->max_num_pages,
			'prev_text' 	=> '&larr;',
			'next_text' 	=> '&rarr;',
			'type'			=> 'list',
			'end_size'		=> 3,
			'mid_size'		=> 3
		) );

	echo '</nav>';
} /* end page navi */




/*********************
RANDOM CLEANUP ITEMS
*********************/

// remove the p from around imgs
function bones_filter_ptags_on_images($content){
	return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

// Edit the excerpt link
function riz3_excerpt_more($more) {
	global $post;
	return '...  <a class="excerpt-read-more" href="'. get_permalink($post->ID) . '" title="'. __( 'Read', 'riz3theme' ) . get_the_title($post->ID).'">'. __( 'Read more', 'riz3theme' ) .'</a>';
}


/*
 * This is a modified the_author_posts_link() which just returns the link.
 *
 * This is necessary to allow usage of the usual l10n process with printf().
 */
function bones_get_the_author_posts_link() {
	global $authordata;
	if ( !is_object( $authordata ) )
		return false;
	$link = sprintf(
		'<a href="%1$s" title="%2$s" rel="author">%3$s</a>',
		get_author_posts_url( $authordata->ID, $authordata->user_nicename ),
		esc_attr( sprintf( __( 'Posts by %s' ), get_the_author() ) ), // No further l10n needed, core will take care of this one
		get_the_author()
	);
	return $link;
}

?>
