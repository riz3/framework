<?php get_header(); ?>
<div id="page-container-event">

  <?php
  $page_title = event_get_options( 'event_title' );
  $page_sub_title = event_get_options( 'event_sub_title' );
  $banner = event_get_options( 'event_image' );
  ?>

  <div class="wrap">
    <div class="page-banner">
      <?php if(!empty($banner)){ ?>
        <img src="<?php echo $banner ?>"/>
      <?php } else { ?>
        <img src="<?php echo get_template_directory_uri() . '/library/images/event_default_banner.jpg'?>" />
      <?php } ?>
    </div><!-- .page-banner -->
    <h1 class="page-title-v2">
      <?php
      if (!empty($page_title)){
        echo $page_title;
      } else {
        echo 'Events';
      }?>
    </h1>
  </div><!-- .wrap -->

  <div class="event-content-container">
    <div class="wrap-sm">

      <?php
      if(!empty($page_sub_title)){
        echo '<h2 class="page-sub-title">' . $page_sub_title . '</h2>';
      } else {
      }?>

      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="card-event">
          <div class="event-date">
            <?php the_date(); ?>
          </div><!-- .event-date -->
          <div class="event-title">
            <h3><?php the_title(); ?></h3>
          </div><!-- .event-title -->
          <div class="event-button">
            <button>View Event Info</button>
          </div><!-- .event-button -->
        </div><!-- .card-event -->
      <?php endwhile; else : ?>
        <article id="post-not-found" class="hentry clearfix">
          <header class="article-header">
              <h1><?php _e( 'Hmmm, what are you looking for?', 'riz3theme' ); ?></h1>
          </header>
          <section class="entry-content">
              <p><?php _e( 'Nothing was found based on your search.', 'riz3theme' ); ?></p>
          </section>
          <footer class="article-footer">
                  <p><?php _e( 'This is the error message in the page.php template.', 'riz3theme' ); ?></p>
          </footer>
        </article>
      <?php endif; ?>

    </div><!-- .wrap -->
  </div><!-- .event-content-container -->
</div><!-- #page-container-->
<?php get_footer(); ?>
