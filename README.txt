=== RIZ3 ===
Contributors: RIZ3
Requires at least: WordPress 4.2
Tested up to: WordPress 4.8.1
Version: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: Responsive, framework, sass, scss, jquery

== Description ==

This is a Wordpress theme created as a framework to be used with a child theme.

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Upload the zipped Riz3 theme
3. Click on the 'Activate' button to use your new theme right away.
4. Create a child theme and upload this to the plugin directory (this is not required but is highly recommended)
    - You may name the child theme anything you like
    - Style.css and functions.php files must be included in your child theme in order for it to work.

== Features ==

SASS
 - This theme uses SASS for styling. DO NOT edit the style.css file in the root directory.
 - Navigate to /library/scss
 - Make all of your edits in the appropriate files in this directory and compile to ../css
 - Media queries are setup in /library/scss/style.scss
 - Child themes should follow the same directory structure

Functions
 - The core functions are located in /library/core-functions.php
 - Enquee and load all stylesheets here along with any javascript/jQuery
 - Add theme specific functions to a child theme

Javascript
  - Place all js, jquery, etc. in /library/js
  - Custom js should be included in /library/js/scripts.js
  - Compile minified files for production to /library/js/min

CMB2 (plugin)
  - This should be use to create custom fields
  - A working example-functions.php file is included in this directory for reference

== Copyright ==

Riz3 WordPress Theme, Copyright 2017
Riz3 is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

Riz3 bundles the following third-party resources:

jQuery FitText, Copyright 2011 Dave Rupert
License: MIT
Source: https://github.com/davatron5000/FitText.js

normalize.css, Copyright 2012-2016 Nicolas Gallagher and Jonathan Neal
License: MIT
Source: https://necolas.github.io/normalize.css/

Font Awesome icons, Copyright Dave Gandy
License: SIL Open Font License, version 1.1.
Source: http://fontawesome.io/


== Changelog ==

Initial release
