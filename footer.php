
</div><!-- #inner-page-container -->

<footer class="footer" role="contentinfo">
    <div class="wrap">
    	<span class="source-org copyright">&copy; <?php echo date('Y'); ?> <a href="<?php echo home_url();?>"><?php bloginfo( 'name' ); ?></a> All Rights Reserved.</span>
    </div><!-- .wrap -->
</footer>

<?php // all js scripts are loaded in library/core-functions.php ?>
<?php wp_footer(); ?>

</body>
</html>
