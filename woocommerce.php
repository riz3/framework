<?php
/**
 * The template woocommerce
 *
 * @package
 * @subpackage
 * @since
 */

get_header(); ?>

<div class="wrap">
  <?php woocommerce_content(); ?>
</div>

<?php get_footer(); ?>
