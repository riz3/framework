<?php
/**
 * The template for displaying single posts
 *
 *
 * @package
 * @subpackage
 * @since
 */

 get_header(); ?>

<div id="page-container">
    <div class="blog-banner" style="background-image: url('<?php echo the_post_thumbnail_url('full'); ?>')">
        <div class="overlay"></div><!-- .overlay -->
        <div class="wrap">
            <h1><?php the_title(); ?></h1>
        </div><!-- .wrap -->
    </div><!-- .blog-banner -->

    <div class="blog-content-container single-post">
    <div class="wrap">
        <div class="row">
            <div class="col-2-3">

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                        <section class="entry-content clearfix">
                            <?php the_content(); ?>
                        </section>

                        <footer class="article-footer">
                            <h5>CATEGORIES:</h5>
                            <?php echo get_the_category_list(); ?>
                        </footer>
                    </article><!-- #post-## -->

                <?php endwhile; ?>
                <?php else : ?>
                    <article id="post-not-found" class="hentry clearfix">
                            <header class="article-header">
                                <h1>This Is Embarrassing :/</h1>
                            </header>
                            <section class="entry-content">
                                <p>The post you were looking for couldn't be found. If this problem persists please contact us.</p>
                            </section>
                            <footer class="article-footer">
                                <p><em>This an the error message for single posts.</em></p>
                            </footer>
                    </article>
                <?php endif; ?>
                <div class="below-content-container">
                    <div class="pagination single-pagination clearfix">
                        <div class="prev-post">
                            <?php
                            if( get_adjacent_post(false, '', true) ) {
                                previous_post_link('%link', '<span>< Previous</span>');
                            } else {
                                $first = new WP_Query('posts_per_page=1&order=DESC'); $first->the_post();
                                    echo '<a href="' . get_permalink() . '"><span>< Previous</span></a>';
                                wp_reset_query();
                            };?>
                        </div><!-- .prev-post -->
                        <div class="next-post">
                            <?php
                            if( get_adjacent_post(false, '', false) ) {
                            	next_post_link('%link', '<span>Next ></span>');
                            } else {
                            	$last = new WP_Query('posts_per_page=1&order=ASC'); $last->the_post();
                                	echo '<a href="' . get_permalink() . '"><span>Next ></span></a>';
                                wp_reset_query();
                            };?>
                        </div><!-- .next-post -->
                    </div><!-- .pagination -->

                    <?php comments_template(); // uncomment if you want to use them ?>
                </div><!-- .below-content-container-->
            </div><!-- .col-2-3 -->

            <div class="col-1-3">
                <?php get_sidebar(); ?>
            </div><!-- .col-1-3 -->

        </div><!-- .row -->
    </div><!-- .wrap -->
</div>
</div><!-- #page-container -->

<?php get_footer(); ?>
