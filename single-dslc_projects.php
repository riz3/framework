<?php
/**
 * The template for displaying single posts
 *
 *
 * @package
 * @subpackage
 * @since
 */

 get_header(); ?>

<div id="page-container">

    <div class="single-post">
    <div class="wrap">
        <div class="row">
            <div class="col-full">
                <h1><?php the_title(); ?></h1>
                <div class="portfolio-featured-image">
                    <?php echo the_post_thumbnail('full'); ?>
                </div><!-- .featured-image -->
            </div><!-- .col-1-3 -->
        </div>
        <div class="row">
            <div class="col-1-4">
                <div>
                    <?php echo riz3_custom_taxonomies_terms_links(); ?>
                </div>
                <div>
                    <h4><a href="../../web-design">< View All Projects</a></h4>
                </div>

            </div><!-- .col-1-4 -->
            <div class="col-3-4">

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                        <section class="entry-content clearfix">
                            <h2>Project Overview:</h2>
                            <?php the_content(); ?>
                        </section>

                        <footer class="article-footer">
                        </footer>
                    </article><!-- #post-## -->

                <?php endwhile; ?>
                <?php else : ?>
                    <article id="post-not-found" class="hentry clearfix">
                            <header class="article-header">
                                <h1>This Is Embarrassing :/</h1>
                            </header>
                            <section class="entry-content">
                                <p>The post you were looking for couldn't be found. If this problem persists please contact us.</p>
                            </section>
                            <footer class="article-footer">
                                <p><em>This an the error message for single posts.</em></p>
                            </footer>
                    </article>
                <?php endif; ?>
                <div class="below-content-container">
                    <div class="pagination single-pagination clearfix">
                        <div class="prev-post">
                            <?php
                            if( get_adjacent_post(false, '', true) ) {
                                previous_post_link('%link', '<span>< Previous</span>');
                            } else {
                                $first = new WP_Query('posts_per_page=1&order=DESC&post_type=dslc_projects'); $first->the_post();
                                    echo '<a href="' . get_permalink() . '"><span>< Previous</span></a>';
                                wp_reset_query();
                            };?>
                        </div><!-- .prev-post -->
                        <div class="next-post">
                            <?php
                            if( get_adjacent_post(false, '', false) ) {
                            	next_post_link('%link', '<span>Next ></span>');
                            } else {
                            	$last = new WP_Query('posts_per_page=1&order=ASC&post_type=dslc_projects'); $last->the_post();
                                	echo '<a href="' . get_permalink() . '"><span>Next ></span></a>';
                                wp_reset_query();
                            };?>
                        </div><!-- .next-post -->
                    </div><!-- .pagination -->

                </div><!-- .below-content-container-->
            </div><!-- .col-3-4 -->

        </div><!-- .row -->
    </div><!-- .wrap -->
</div>
</div><!-- #page-container -->

<?php get_footer(); ?>
