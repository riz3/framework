<?php 
/**
 * The template for 404 error
 *
 * @package
 * @subpackage
 * @since
 */

get_header(); ?>


<div id="content">
    <div id="inner-content" class="wrap clearfix">
        <div class="col-8">
            <article id="post-not-found" class="hentry clearfix">
                <header class="article-header">
                    <h1><?php _e( '404 - Not Found', 'bonestheme' ); ?></h1>
                </header>
                <section class="entry-content">
                    <p><?php _e( 'I am sorry but what you were looking for was not found, but maybe try looking again!', 'bonestheme' ); ?></p>
                </section>

                <section class="search">
                    <p><?php get_search_form(); ?></p>
                </section>

                <footer class="article-footer">
                    <p><?php _e( 'This is the 404.php template.', 'bonestheme' ); ?></p>
                </footer>
            </article>
        </div><!-- .col-8 -->
    </div><!-- #inner-content -->
</div><!-- #content -->

<?php get_footer(); ?>
