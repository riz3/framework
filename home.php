<?php

/**
 *
 * @package
 * @subpackage
 * @since
 */


get_header(); ?>

<div id="blog-page-container">

  <div class="blog-title-container">
    <h1 class="blog-title">Blog</h1>
    <hr class="blog-line">
  </div><!-- .blog-title-container -->

    <div class="blog-content-container">
        <div class="wrap">
            <div class="row">
                <div class="col-2-3">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                      <a class="thumbnail-link" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('large'); ?>
                        <span class="overlay"><button>Read More</button></span>
                      </a>
                        <div class="post-excerpt-container">
                            <div class="share-container">
                              <span class="share-title">Share</span>
                              <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank">
                                <span class="icons icon-facebook2"></span>
                              </a>
                            </div><!-- .share-container -->

                            <article>
                              <header class="article-header">
                                <div class="category-links">
                                  <?php the_category(', '); ?>
                                </div><!-- .category-links -->
                                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                              </header>

                              <section class="entry-content">
                                <?php the_excerpt(); ?>
                              </section>

                              <footer class="article-footer date-container">
                                <?php the_date(); ?>
                              </footer>
                            </article>
                        </div><!-- .post-excerpt-container -->
                    <?php endwhile; else : ?>
                        <article id="post-not-found" class="hentry clearfix">
                            <header class="article-header">
                                <h1><?php _e( 'Hmmm, what are you looking for?', 'riz3theme' ); ?></h1>
                            </header>
                            <section class="entry-content">
                                <p><?php _e( 'Nothing was found based on your search.', 'riz3theme' ); ?></p>
                            </section>
                            <footer class="article-footer">
                                    <p><?php _e( 'This is the error message in the page.php template.', 'riz3theme' ); ?></p>
                            </footer>
                        </article>
                    <?php endif; ?>
                </div><!-- .col-2-3 -->
                <div class="col-1-3">
                    <?php get_sidebar(); ?>
                </div><!-- .col-1-3 -->
            </div><!-- .row-->
        </div><!-- .wrap -->
    </div><!-- .blog-content-container -->
</div><!-- #page-container-->

<?php get_footer(); ?>
