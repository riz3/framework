<?php get_header(); ?>

<div id="page-container">
    <div class="row">
        <div class="col-2-3">
        
        	<h1>See What's Trending</h1>

            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article">

                    <header class="article-header">
                        <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                        <h4 class="byline vcard"><?php
                            printf( __( 'Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time>', 'bonestheme' ), get_the_time('Y-m-j'), get_the_time(get_option('date_format')));
                        ?></h4>
                    </header>

                    <section class="entry-content clearfix">
                        <?php the_excerpt(); ?>
                    </section>

                    <footer class="article-footer">
                        <p class="tags"><?php the_tags( '<span class="tags-title">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '' ); ?></p>
                    </footer>

                    <?php // comments_template(); // uncomment if you want to use them ?>

                </article>
            <?php endwhile; ?>
            <?php if ( function_exists( 'bones_page_navi' ) ) { ?>
                    <?php bones_page_navi(); ?>
            <?php } else { ?>
                <nav class="wp-prev-next">
                    <ul class="clearfix">
                        <li class="prev-link"><?php next_posts_link( __( '&laquo; Older Entries', 'bonestheme' )) ?></li>
                        <li class="next-link"><?php previous_posts_link( __( 'Newer Entries &raquo;', 'bonestheme' )) ?></li>
                    </ul>
                </nav>
            <?php } ?>
            <?php else : ?>
                <article id="post-not-found" class="hentry clearfix">
                    <header class="article-header">
                        <h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
                    </header>
                    <section class="entry-content">
                        <p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
                    </section>
                    <footer class="article-footer">
                        <p><?php _e( 'This is the error message in the index.php template.', 'bonestheme' ); ?></p>
                    </footer>
                </article>
            <?php endif; ?>
        </div><!-- .col-2-3 -->
        
        <div class="col-1-3">
        </div><!-- .col-1-3 -->
        
    </div><!-- .row -->
</div><!-- #page-container -->

<?php get_footer(); ?>
