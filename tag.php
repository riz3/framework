<?php
/**
 * The template for displaying Tag pages
 *
 * Used to display archive-type pages for posts in a tag.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package
 * @subpackage
 * @since
 */

get_header(); ?>

<div id="content">
	<div id="inner-content" class="wrap clearfix">
    	<div class="row">
        
        	<div class="col-8">
                <section id="primary" class="content-area">
                        <?php if ( have_posts() ) : ?>
            
                        <header class="archive-header">
                            <h1 class="archive-title"><?php printf( __( 'Tag Archives: %s'), single_tag_title( '', false ) ); ?></h1>
            
                            <?php
                                // Show an optional term description.
                                $term_description = term_description();
                                if ( ! empty( $term_description ) ) :
                                    printf( '<div class="taxonomy-description">%s</div>', $term_description );
                                endif;
                            ?>
                        </header><!-- .archive-header -->
            
                        <?php
                                // Start the Loop.
                                while ( have_posts() ) : the_post();
            
                                    /*
                                     * Include the post format-specific template for the content. If you want to
                                     * use this in a child theme, then include a file called called content-___.php
                                     * (where ___ is the post format) and that will be used instead.
                                     */
                                    get_template_part( 'content', get_post_format() );
            
                                endwhile; ?>
                                
                                <!-- Previous/next page navigation -->
                                <nav class="wp-prev-next">
                                    <ul class="clearfix">
                                        <li class="prev-link"><?php next_posts_link( __( '&laquo; Older Entries')) ?></li>
                                        <li class="next-link"><?php previous_posts_link( __( 'Newer Entries &raquo;')) ?></li>
                                    </ul>
                                </nav>
            
                           <?php  else :
                                // If no content, include the "No posts found" template.
                                get_template_part( 'content', 'none' );
            
                            endif;
                        ?>
                </section><!-- #primary -->
        	</div><!-- .col-8 -->
            
            <div class="col-4">
            	<!-- Insert sidebar or something awesome -->
            </div><!-- .col-4 -->
    
		</div><!-- .row -->
	</div><!-- #inner-content -->
</div><!-- #content -->

<?php get_footer(); ?>