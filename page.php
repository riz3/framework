<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package
 * @subpackage
 * @since
 */

get_header(); ?>

<div id="page-container">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="wrap">
      <div class="page-banner">
        <?php the_post_thumbnail('full');?>
      </div><!-- .page-banner -->
      <article>
        <header class="article-header">
          <h1 class="page-title"><?php the_title(); ?></h1>
        </header>
        <section class="entry-content">
          <?php the_content(); ?>
        </section>
        <footer class="article-footer">
        </footer>
      </article>
    </div><!-- .wrap -->
  <?php endwhile; else : ?>
    <div class="wrap">
      <article id="post-not-found" class="hentry clearfix">
          <header class="article-header">
              <h1><?php _e( 'Hmmm, what are you looking for?', 'riz3theme' ); ?></h1>
          </header>
          <section class="entry-content">
              <p><?php _e( 'Nothing was found based on your search.', 'riz3theme' ); ?></p>
          </section>
            <footer class="article-footer">
                  <p><?php _e( 'This is the error message in the page.php template.', 'riz3theme' ); ?></p>
          </footer>
      </article>
    </div><!-- .wrap -->
  <?php endif; ?>
</div><!-- #page-container-->

<?php get_footer(); ?>
